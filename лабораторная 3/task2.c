//
//  task2.c
//  third_lab
//
//  Created by Eugene Gusev on 09.06.14.
//  Copyright (c) 2014 Eugene Gusev. All rights reserved.
//

#include <stdio.h>
#include <string.h>

int main () {
    int start=0, end, c=0, i=0;
    char str[256], nonsymb[256];
    printf("Enter a string: ");
    fgets(str,256,stdin);
    while (str[i]!='\n' && str[i]!='\0') {
        if (str[i]>='a' && str[i]<='z') {
            nonsymb[c]=str[i];
            c++;
        }
        if (str[i]>='A' && str[i]<='Z') {
            nonsymb[c]=str[i];
            c++;
        }
        i++;
    }
    end=strlen(nonsymb)-1;
    while (start<end) {
        if (nonsymb[start]!=nonsymb[end]) {
            puts("No");
            return 0;
        }
        start++;
        end--;
    }
    puts("Yes");
    return 0;
}