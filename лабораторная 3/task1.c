//
//  task1.c
//  third_lab
//
//  Created by Eugene Gusev on 16.06.14.
//  Copyright (c) 2014 Eugene Gusev. All rights reserved.
//

#include <stdio.h>

int main () {
    int max_len=6, count=0, x=0, number=0, sum=0;
    char str[256];
    printf("Enter a string of numbers: ");
    fgets(str, 256, stdin);
    while (str[x]) {
        if (str[x]>='0' && str[x]<='9') {
            if (count < max_len) {
                number=number*10+(str[x]-'0');
                count++;
            }
            else {
                sum= sum+ number;
                count=0;
                number=str[x]-'0';
            }
            
        }
        else {
            count=0;
            sum= sum+ number;
            number=0;
        }
        x++;
    }
    printf("%d \n", sum);
    return 0;
}