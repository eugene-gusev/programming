//
//  task4
//  third_lab
//
//  Created by Eugene Gusev on 26.05.14.
//  Copyright (c) 2014 Eugene Gusev. All rights reserved.
//
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, const char * argv[])
{
    int family = 0, maxAge = 0, minAge;
    puts("Enter a quantity of your relatives: ");
    scanf("%d", &family);
    char buf[100][256];
    char *old, *young;
    int age[100], i = 0;
    while (i<family) {
        printf ("Enter name: ");
        scanf("%s", &buf[i]);
        printf ("Enter age: ");
        scanf("%d", &age[i]);
        minAge=age[0];
        if (age[i]>maxAge) {
                old=&buf[i];
            maxAge=age[i];
            }
        if (age[i]<minAge) {
                young=&buf[i];
            minAge=age[i];
        }
        i++;
    }
    printf("%s is oldest\n", old);
    printf("%s is youngest\n", young);
    return 0;
}

