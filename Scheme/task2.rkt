(define (triangle n)
  (if (= n 1)
      1
      (+ n (triangle (- n 1)))))

(define (sum-numb n)
  (if (= n 0)
      0
      (+ (sum-numb (floor (/ n 10))) (modulo n 10))))