(define task 1000)
(define upper-limit (/ task 2))

(define (prime? p)
  (define (non-divisible-by n d)
    (cond
     ((= d 1) #t)
     (else (if(= (remainder n d) 0)
          #f
          (non-divisible-by n (- d 1))))))
  (if (= p 1)
      #t
      (non-divisible-by p (- p 1))))


(define (makelist n)
  (if (= n 2)
     (list 2)
     (if (prime? n)
     (cons n (makelist (- n 1)))
      (makelist (- n 1)))))

(define (sum elemList)
  (if
    (null? elemList)
    0
    (+ (car elemList) (sum (cdr elemList)))))


(define (final n)
  (if(< (sum(sort(makelist n)\>)) task )
     (sum(sort(makelist n)\>))
     (final (- n 1))))

(define answer (final upper-limit))