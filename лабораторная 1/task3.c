//
//  task3.c
//  lab
//
//  Created by Eugene Gusev on 13.04.14.
//  Copyright (c) 2014 Eugene Gusev. All rights reserved.
//

#include <stdio.h>
#include <string.h>

int main () {
    int i = 0;
    char str[]={"a193jfn746bd32j"};
    for (;str[i]!='\0'; i++) {
        if (str[i]>='a' && str[i]<='z') {
            printf("%c",str[i]);
        }
    }
    i=0;
    printf(" ");
    for (;str[i]!='\0'; i++) {
        if (str[i]>='0' && str[i]<='9') {
            printf("%c",str[i]);
        }
    }
    printf("\n");
    return 0;
}