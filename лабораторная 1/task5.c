//
//  task5.c
//  lab
//
//  Created by Eugene Gusev on 13.04.14.
//  Copyright (c) 2014 Eugene Gusev. All rights reserved.
//

#include <stdio.h>

int main() {
    int space, user, star=1;
    printf("Enter a number (f.e. 5): ");
    scanf("%d",&user);
    space=user;
    while (space>0) {
        for (int a=space; a>0; a--) {
            printf(" ");
        }
        for (int b=star; b>=1; b--){
            printf("*");
        }
        printf("\n");
        space--;
        star=star+2;
    }
    //star 9, space 0
    while (space<=user) {
        for (int c=0; c<space; c++) {
            printf(" ");
        }
        for (int d=0; d<star; d++){
            printf("*");
        }
        printf("\n");
        star=star-2;
        space++;
    }
    return 0;
}